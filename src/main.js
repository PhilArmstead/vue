/** App imports **/
// Vue + router(?)
import Vue from 'vue';
//import VueRouter from 'vue-router';

// Our app
import App from './vue/App';

// Our app's pages (for router
//import PageHome from './vue/page/PageHome';

// Our stylesheet
import './sass/style.scss'



// Configure router
//Vue.use(VueRouter);
//const router = new VueRouter({
//	mode: 'history', // Why is this not default?!
//	routes: [
//		{name: 'home', path: '*', component: PageHome},
//	]
//});


// Render this biznitch.
new Vue({
	el: '#app',
	render: h => h(App),
	//router
});