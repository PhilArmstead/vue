const w = window;
const d = document;

export const browser = (function () {
	const n = navigator ? navigator : w.navigator;
	const ua = n.userAgent;

	return {
		is_opera: typeof w.opr !== 'undefined',
		is_chrome: ~ua.indexOf('Chrome') || ~ua.indexOf('CriOS'),
		is_explorer: ~ua.indexOf('MSIE') || ~ua.indexOf('Trident/'),
		is_firefox: ~ua.indexOf('Firefox') || ~ua.indexOf('FxiOS'),
		is_safari: ~ua.indexOf('Safari'),
		is_edge: ~ua.indexOf('Edge'),
		is_ipad: ~ua.indexOf('iPad'),
		is_windows: /Windows NT/i.test(ua),
		is_osx: !!n.platform && /MacIntel/i.test(n.platform),
		is_ios: !!n.platform && /iPad|iPhone|iPod/i.test(n.platform),
		is_android: /android/i.test(ua),
		is_windows_phone: /Windows Phone/i.test(ua),
		is_mobile: /mobile/i.test(ua),
		is_facebook: ~ua.indexOf('FBAN') || ~ua.indexOf('FBAV'),
		is_twitter: ~ua.indexOf('Twitter'),
		is_instagram: ~ua.indexOf('Instagram'),
		has_touch: has_touch(),
	};
})();

browser.is_chrome = browser.is_chrome && !browser.is_opera && !browser.is_facebook && !browser.is_edge;
browser.is_explorer = browser.is_explorer && !browser.is_facebook;
browser.is_safari = browser.is_safari && !browser.is_chrome && !browser.is_android && !browser.is_edge;
browser.is_mobile = browser.is_mobile || browser.is_ipad || browser.is_ios || browser.is_android || browser.is_windows_phone;
browser.is_in_app_browser = browser.is_facebook || browser.is_instagram || browser.is_twitter;


// Bind browser properties to <html> element
let $html = d.querySelector('html');
for (let i in browser) {
	if (browser[i]) {
		$html.classList.add(i.replace(/_/g, '-'));
	}
}




/*** Library ***/

// Create or create DOM elements (like jQuery)
export function $(selector, subselector) {

	// Are we looking for elements or creating one?
	return typeof selector === 'string' && selector.trim().match(/<(\w+)(.*?)\/?>/) ?
		create_element(selector, subselector) : // Creating
		find_element(selector, subselector); // Looking

}

function find_element(selector, subselector) {

	const slice = [].slice;
	const lookup = function (s) {
		if (/^([#|.]?[\w-]+)$/.test(s)) {
			switch (s.charAt(0)) {
				case '#': // Handle ID-based selectors
					return d.getElementById(s.substr(1));
				case '.': // Handle class-based selectors
					return slice.call(selector.getElementsByClassName(s.substr(1)));
				default: // Handle tag-based selectors
					return slice.call(selector.getElementsByTagName(s));
			}
		} else {
			return slice.call(selector.querySelectorAll(s)); // Default to querySelectorAll
		}
	};

	// If we've passed $(parent, '.child'), then re-jig some variables
	if (subselector === undefined) {
		subselector = selector;
		selector = d;
	}

	// Skip the loop and return only a single node if we're looking for something by ID
	if (/^#[\w-]+$/.test(subselector)) {
		return d.getElementById(subselector.substr(1));
	}

	// Explode (possible) list of selectors, loop through them, add to return list
	return flatten_array(subselector.split(',').map(lookup));

}

function create_element(selector, options) {

	//  Create empty element and fill it with our markup
	const $template = d.createElement('template');
	$template.insertAdjacentHTML('beforeend', selector);

	// Get the inserted markup as a DOM element
	const $element = $template.firstElementChild;

	// Apply options to $element
	if (typeof options === 'object') {
		Object.keys(options).forEach(function (x) {
			$element.setAttribute(x, options[x]);
		});
	}

	return $element;
}

function flatten_array(arr) {
	return [].concat.apply([], arr);
}



// Bind events to DOM Elements
Window.prototype.on = Element.prototype.on = document.on = function (event, callback, options) {

	if (!options) {
		options = {
			passive: true,
			once: false,
		};
	}

	const $this = this;

	event.replace(/(\s)/g, '').split(',').forEach(function (event) {
		if ($this === d && event === 'load') {
			event = 'DOMContentLoaded';
		}

		$this.addEventListener(event, callback, options);
	});

	return this;
};


// Bind events to elements in arrays
//Array.prototype.on = function (event, callback) {
//	return this.map(function (node) {
//		return node.on(event, callback);
//	});
//};


//Window.prototype.off = Element.prototype.off = document.off = function (event, callback) {
//
//	const $this = this;
//
//	event.replace(/(\s)/g, '').split(',').forEach(function (event) {
//		if ($this === d && event === 'load') {
//			event = 'DOMContentLoaded';
//		}
//
//		$this.removeEventListener(event, callback);
//	});
//
//	return this;
//};
//
//
//Array.prototype.off = function (event, callback) {
//	return this.map(function (node) {
//		return node.off(event, callback);
//	});
//};


//Array.prototype.dedupe = function () {
//	let ret_arr = [];
//
//	this.forEach(function (element) {
//		element = element.trim ? element.trim() : element;
//
//		if (ret_arr.indexOf(element) === -1) {
//			ret_arr.push(element);
//		}
//	});
//
//	return ret_arr;
//
//};

//Array.prototype.unset = function (index) {
//	if (typeof this[index] !== 'undefined') {
//		this.splice(index, 1);
//	}
//};


//function wrap_x_around_y($wrapper, $element) {
//	let $wrap = $('<div>');
//
//	$wrap.innerHTML = $element.outerHTML;
//
//	$wrapper.appendChild($wrap.firstElementChild);
//
//	$element.parentElement.insertBefore($wrapper, $element);
//	$element.remove();
//
//}



/** Load a URL or post data to it
 * * * * * * * * * * * * * * * * *
 options = {
		method: 'GET', 	// 'PUT' | 'DELETE' | 'POST'
		callback: null, // function() { }
		headers: null,	// {header_1: 'Foo', header_2: 'rar'}
		body: null			// {var_1: 'Foo', var_2: 'lol'}
		json: null			// {var_1: 'Foo', var_2: 'lol'}
		form_data: null	// {var_1: 'Foo', var_2: 'lol'}
 	}
 */
//export function load(src, options) {
//
//	let request = new XMLHttpRequest();
//
//	options = Object.assign({
//		method: !options.method && (options.body || options.json || options.form_data) ? 'POST' : 'GET',
//		callback: false,
//		headers: {},
//		body: false,
//		json: false,
//		form_data: false,
//		responseType: 'text',
//	}, options);
//
//
//	// If callback was supplied, attach it to onload event
//	// `onload` implies success, as opposed to `onreadystatechange` which requires a check
//	if (options.callback && options.callback.call) {
//		request.onload = function () {
//			options.callback(options.responseType === 'text' ? request.responseText : request.response, request);
//		}
//	}
//
//	// Send that mother
//	request.open(options.method, src, true);
//	request.responseType = options.responseType;
//
//
//	// Configure post vars
//	let post_vars = '';
//
//	if (typeof options.form_data === 'object') {
//		if (options.form_data instanceof FormData) {
//			post_vars = options.form_data;
//		} else {
//			post_vars = new FormData();
//
//			for (let key in options.form_data) {
//				post_vars.append(key, options.form_data[key]);
//			}
//		}
//		//options.headers['Content-type'] = 'multipart/form-data';
//	} else if (typeof options.json === 'object') {
//		post_vars = JSON.stringify(options.json);
//		options.headers['Content-type'] = 'application/json';
//	} else if (typeof options.body === 'object') {
//		for (let i in options.body) {
//			post_vars += i + '=' + encodeURIComponent(options.body[i]) + '&';
//		}
//		post_vars = post_vars.substr(0, post_vars.length - 1);
//
//		options.headers['Content-type'] = 'application/x-www-form-urlencoded';
//	}
//
//
//	// Attach headers
//	if (typeof options.headers === 'object') {
//		for (let i in options.headers) {
//			if (options.headers.hasOwnProperty(i)) {
//				request.setRequestHeader(i, options.headers[i]);
//			}
//		}
//	}
//
//	request.send(post_vars);
//}




//Array.prototype.shuffle = function () {
//
//	let
//		counter = this.length
//		, tmp
//		, index
//	;
//
//	while (counter > 0) {
//
//		// Pick a random index
//		index = (Math.random() * counter--) | 0;
//
//		// Swap the last element with it
//		tmp = this[counter];
//		this[counter] = this[index];
//		this[index] = tmp;
//
//	}
//
//};


// Find first ancestor with specific CSS selector
//function find_ancestor($element, selector) {
//
//	if (!$element) {
//		return null;
//	}
//
//	do {
//		if ($element.matches(selector)) {
//			return $element;
//		}
//	} while (($element = $element.parentElement));
//
//	return null;
//
//}


// Set cookies
// Unset by setting days to -1
//function set_cookie(name, value, days) {
//	let expires = '; expires=';
//
//	if (days) {
//		let date = new Date();
//		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//		expires += date.toUTCString();
//	}
//
//	d.cookie = name + '=' + (value || '') + expires + '; domain=.' + d.domain + ';path=/';
//
//}



//function is_numeric(something) {
//	return !Array.isArray(something) && (something - parseFloat(something) + 1) >= 0;
//}




function has_touch() {
	if (('ontouchstart' in w) || w.DocumentTouch && d instanceof DocumentTouch) {
		return true;
	}

	let prefixes = ['', '-webkit-', '-moz-', '-o-', '-ms-'];

	// include the 'heartz' as a way to have a non matching MQ to help terminate the join
	let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');


	return w.matchMedia(query).matches;
}




// Sequentially load array of images [{src: 'rar.png'}, {src:' lolpng', callback: something}]
export function sequentialPreload(images, index) {
	index = index || 0;

	if (images && images.length > index) {
		let img = new Image();
		let next = function () {

			if (images[index].fired) {
				return;
			}

			images[index].fired = 1;

			sequentialPreload(images, index + 1);

			if (images[index].callback && images[index].callback.call) {
				images[index].callback();
			}
		};

		img.onload = next;
		img.onerror = next;
		img.src = images[index].src;
	}
}
