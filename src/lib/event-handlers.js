const w = window;
const s = document.documentElement.style;

// Store window dimensions
export let vw = null;
export let vh = null;

// onResize caches viewport dimensions + stores as CSS variable
export function onResize() {

	// Cache viewport size
	w.vw = w.innerWidth;
	w.vh = w.innerHeight;

	// Set height of window as CSS variable
	s.setProperty('--body-height', w.vh + 'px');

}
