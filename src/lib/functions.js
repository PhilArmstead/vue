//const w = window;
//const d = document;
//const b = d.body;
//
//import {$, browser} from './_lib';


//export function animate(duration, delta, callback, easing) {
//
//	let start = new Date;
//	let advance_frame = function () {
//
//		// progress = time passed / animation duration
//		let progress = (new Date - start) / duration;
//
//		// Progress caps out at 1 (its a fraction, not a percentage)
//		if (progress > 1) {
//			progress = 1;
//		}
//
//		// Modify our progress with a custom easing?
//		if (easing && easing.call) {
//			progress = easing(progress, duration);
//		}
//
//		// Run our delta (and optionally cancel this if the delta function says so
//		let ret = delta(progress);
//
//		// If delta didn't return FALSE and we're not finished, go again
//		if (progress < 1 && ret !== false) {
//			w.requestAnimationFrame(advance_frame);
//		} else if (progress === 1 && callback && callback.call) {
//			callback();
//		}
//
//	};
//
//
//	// Go!
//	w.requestAnimationFrame(advance_frame);
//
//}


//export function newWindow(url, title, width, height, onUnload, onDidNotOpen) {
//	let closed;
//	let left = (window.screen.width / 2) - ((width / 2) + 10);
//	let top = (window.screen.height / 2) - ((height / 2) + 50);
//	let newWindow = window.open(url, title, "status=no,height="
//		+ height + ",width=" + width + ",resizable=1,scrollbars=1,left="
//		+ left + ",top=" + top + ",screenX=" + left + ",screenY="
//		+ top + ",toolbar=no,menubar=no,location=no,directories=no,fullscreen=no");
//
//
//	// Didn't open? Blocked?
//	if (!newWindow || !newWindow.focus) {
//
//		// Exit and possibly run error callback
//		return onDidNotOpen && onDidNotOpen.call ? onDidNotOpen() : false;
//	}
//
//	// It opened. Whoo.
//	newWindow.focus();
//
//	// Listen for onUnload callback
//	if (onUnload && onUnload.call) {
//		closed = setInterval(function () {
//
//			if (newWindow.closed) {
//				clearInterval(closed);
//				onUnload();
//			}
//
//		}, 250);
//	}
//}