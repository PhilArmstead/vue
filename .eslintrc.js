// See https://github.com/vuejs/eslint-plugin-vue

module.exports = {
	extends: [

		// These four options are more or less levels of strictness; the above URL gives an idea of how strict the rules are
		// FYI this is Vue linting, not regular JS linting

		//'plugin:vue/base',
		'plugin:vue/essential',
		'plugin:vue/strongly-recommended',
		//'plugin:vue/recommended',
	],

	// Overrides
	rules: {
		"vue/html-indent": false, // Let's not take a stand on tabs vs spaces today

		// I don't care if people want multiple empty lines in their app
		"vue/multiline-html-element-content-newline": ["warning", {
			"allowEmptyLines": true
		}]
	}
};

